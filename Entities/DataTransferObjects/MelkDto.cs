﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.DataTransferObjects
{
    public class MelkDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string MelkNO { get; set; }
        public string Area { get; set; }
        public string Address { get; set; }
        public string Direction { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsDeleted { get; set; } = false;
        public OwnerDto Owner { get; set; }
    }
}
