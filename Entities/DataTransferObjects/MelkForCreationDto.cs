﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Entities.DataTransferObjects
{
    public class MelkForCreationDto
    {
       [Required(ErrorMessage = "نام الزامیست")]
        [StringLength(60, ErrorMessage = "Name can't be longer than 60 characters")]
        public string Name { get; set; }


        [Required(ErrorMessage = "شماره الزامیست")]
        [StringLength(12, ErrorMessage = "MelkNO can't be longer than 12 characters")]
        public string MelkNO { get; set; }

        [Required(ErrorMessage = "متراژ الزامیست")]
        [StringLength(12, ErrorMessage = "Area can't be longer than 12 characters")]
        public string Area { get; set; }

        [Required(ErrorMessage = "Address is required")]
        [StringLength(100, ErrorMessage = "Address cannot be loner then 100 characters")]
        public string Address { get; set; }

        [Required(ErrorMessage = "جهت الزامیست")]
        [StringLength(20, ErrorMessage = "Direction cannot be loner then 100 characters")]
        public string Direction { get; set; }

        [Required(ErrorMessage = "BY whom was created is required")]        
        public string CreatedBy { get; set; }
        public bool IsDeleted { get; set; } = false;
        public int OwnerId { get; set; }


    }
}
