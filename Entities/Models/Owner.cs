﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("owner")]
    public class Owner
    {
        [Column("OwnerId")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(60, ErrorMessage = "Name can't be longer than 60 characters")]
        public string Name { get; set; }

        [Required(ErrorMessage = "نام خانوادگی الزامیست")]
        [StringLength(60, ErrorMessage = "Name can't be longer than 60 characters")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "تلفن الزامیست")]
        [StringLength(12, ErrorMessage = "PhoneNO can't be longer than 12 characters")]
        public string PhoneNO { get; set; }

        [Required(ErrorMessage = "Date created is required")]
        public DateTime DateCreated
        {
            get
            {
                return this.dateCreated.HasValue
                   ? this.dateCreated.Value
                   : DateTime.Now;
            }

            set { this.dateCreated = value; }
        }

        private DateTime? dateCreated = null;
        public DateTime DateUpdated { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public ICollection<Account> Accounts { get; set; }
        // [Required(ErrorMessage = "Date of birth is required")] 
        // public DateTime DateOfBirth { get; set; }

        // [Required(ErrorMessage = "Address is required")] 
        // [StringLength(100, ErrorMessage = "Address cannot be loner then 100 characters")] 
        // public string Address { get; set; }

        // [Required(ErrorMessage = "Melk Id is required")]

        // [ForeignKey(nameof(Melk))]
        // public int MelkId { get; set; }
        // public Melk Melk { get; set; }
    }
}
