﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("Melk")]
    public class Melk
    {
        [Column("MelkId")]
        public int Id { get; set; }

        [Required(ErrorMessage = "نام الزامیست")]
        [StringLength(60, ErrorMessage = "Name can't be longer than 60 characters")]
        public string Name { get; set; }


        [Required(ErrorMessage = "شماره الزامیست")]
        [StringLength(12, ErrorMessage = "MelkNO can't be longer than 12 characters")]
        public string MelkNO { get; set; }

        [Required(ErrorMessage = "متراژ الزامیست")]
        [StringLength(12, ErrorMessage = "Area can't be longer than 12 characters")]
        public string Area { get; set; }

        [Required(ErrorMessage = "Address is required")]
        [StringLength(100, ErrorMessage = "Address cannot be loner then 100 characters")]
        public string Address { get; set; }

        [Required(ErrorMessage = "جهت الزامیست")]
        [StringLength(20, ErrorMessage = "Direction cannot be loner then 100 characters")]
        public string Direction { get; set; }

        // [Required(ErrorMessage = "Date created is required")]
        public DateTime DateCreated { get; set; }

        // [Required(ErrorMessage = "Date updated is required")]
        public DateTime DateUpdated { get; set; }

        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public bool IsDeleted { get; set; } = false;

        [Required(ErrorMessage = "Owner Id is required")]

        [ForeignKey(nameof(Owner))]
        public int OwnerId { get; set; }
        public Owner Owner { get; set; }
    }
}
