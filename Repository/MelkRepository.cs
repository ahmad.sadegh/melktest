﻿using Contracts;
using Entities;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Repository
{
    public class MelkRepository : RepositoryBase<Melk>, IMelkRepository
    {
        public MelkRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public IEnumerable<Melk> GetAllMelks()
        {
            return FindAll()
                .Include(ow => ow.Owner)
                .OrderBy(ow => ow.Name)
                .ToList();
        }

        public Melk GetMelkById(int MelkId)
        {
            return FindByCondition(Melk => Melk.Id.Equals(MelkId))
                .Include(ow => ow.Owner)
                .FirstOrDefault();
        }

        public Melk GetMelkWithDetails(int MelkId)
        {
            return FindByCondition(Melk => Melk.Id.Equals(MelkId))
                .Include(ow => ow.Owner)
                .FirstOrDefault();
        }

        public void CreateMelk(Melk Melk)
        {
            try
            {
                Create(Melk);
            }
            catch (System.Exception e)
            {
                // Console.Write(e)
            }
        }

        public void UpdateMelk(Melk Melk)
        {
            try
            {
                Update(Melk);
            }
            catch (System.Exception e)
            {
            }
        }

        public void DeleteMelk(Melk Melk)
        {
            Delete(Melk);
        }
    }
}
