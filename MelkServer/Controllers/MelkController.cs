﻿using AutoMapper;
using Contracts;
using Entities.DataTransferObjects;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MelkServer.Controllers
{
    [Route("api/melk")]
    [ApiController]
    public class MelkController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;

        public MelkController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {
            _logger = logger;
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetAllMelks()
        {
            try
            {
                var melks = _repository.Melk.GetAllMelks();
                _logger.LogInfo($"Returned all melks from database.");

                var melksResult = _mapper.Map<IEnumerable<MelkDto>>(melks);
                return Ok(melksResult);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllmelks action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("{id}", Name = "MelkById")]
        public IActionResult GetMelkById(int id)
        {
            try
            {
                var melk = _repository.Melk.GetMelkById(id);
                if (melk == null)
                {
                    _logger.LogError($"melk with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned melk with id: {id}");

                    var melkResult = _mapper.Map<MelkDto>(melk);
                    return Ok(melkResult);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetMelkById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("{id}/melkWithDetails")]
        public IActionResult GetMelkWithDetails(int id)
        {
            try
            {
                var melk = _repository.Melk.GetMelkWithDetails(id);
                if (melk == null)
                {
                    _logger.LogError($"Melk with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned melk with details for id: {id}");

                    var melkResult = _mapper.Map<MelkDto>(melk);
                    return Ok(melkResult);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetMelkWithDetails action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        public IActionResult CreateMelk([FromBody] MelkForCreationDto melk)
        {
            try
            {
                if (melk == null)
                {
                    _logger.LogError("Melk object sent from client is null.");
                    return BadRequest("Melk object is null");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid melk object sent from client.");
                    return BadRequest("Invalid model object");
                }
                var melkEntity = _mapper.Map<Melk>(melk);
                melkEntity.DateCreated=DateTime.Now; 

                _repository.Melk.CreateMelk(melkEntity);
                _repository.Save();

                var createdMelk = _mapper.Map<MelkDto>(melkEntity);

                return CreatedAtRoute("MelkById", new { id = createdMelk.Id }, createdMelk);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateMelk action: {ex.Message}");
                _logger.LogInfo($"Returned melk with details for id: {ex.InnerException}");

                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPut("{id}")]
        public IActionResult UpdateMelk(int id, [FromBody] MelkForUpdateDto melk)
        {
            try
            {
                if (melk == null)
                {
                    _logger.LogError("Melk object sent from client is null.");
                    return BadRequest("Melk object is null");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid melk object sent from client.");
                    return BadRequest("Invalid model object");
                }

                var melkEntity = _repository.Melk.GetMelkById(id);
                if (melkEntity == null)
                {
                    _logger.LogError($"Melk with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _mapper.Map(melk, melkEntity);
                melkEntity.DateUpdated=DateTime.Now; 
                
                _repository.Melk.UpdateMelk(melkEntity);
                _repository.Save();

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateMelk action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpDelete("{id}/{updaterName}")]
        public IActionResult DeleteMelk(int id,string updaterName)
        {
            try
            {
                var melkEntity = _repository.Melk.GetMelkById(id);
                if (melkEntity == null)
                {
                    _logger.LogError($"Melk with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                
                melkEntity.IsDeleted=true; 
                melkEntity.UpdatedBy=updaterName; 
                melkEntity.DateUpdated=DateTime.Now; 
                _repository.Melk.UpdateMelk(melkEntity);
                _repository.Save();

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteMelk action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}