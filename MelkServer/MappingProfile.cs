﻿using AutoMapper;
using Entities.DataTransferObjects;
using Entities.Models;
using System.Linq;

namespace MelkServer
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Owner, OwnerDto>();

            CreateMap<Account, AccountDto>();

            CreateMap<OwnerForCreationDto, Owner>();

            CreateMap<OwnerForUpdateDto, Owner>();


            CreateMap<Melk, MelkDto>();

            CreateMap<MelkForCreationDto, Melk>();

            CreateMap<MelkForUpdateDto, Melk>() 
            .ForAllMembers(opt => opt.Condition((source, dest, sourceMember, destMember) => (sourceMember != null)));
        }
    }
}
