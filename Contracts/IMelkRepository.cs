﻿using Entities.Models;
using System;
using System.Collections.Generic;

namespace Contracts
{
    public interface IMelkRepository : IRepositoryBase<Melk>
    {
        IEnumerable<Melk> GetAllMelks();
        Melk GetMelkById(int MelkId);
        Melk GetMelkWithDetails(int MelkId);
        void CreateMelk(Melk Melk);
        void UpdateMelk(Melk Melk);
        void DeleteMelk(Melk Melk);
    }
}
